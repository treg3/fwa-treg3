<?php

namespace app\components;

use Yii;
use yii\base\Component;


/**
 * Description of UserStatus
 *
 * @author Misbahul D Munir <misbahuldmunir@gmail.com>
 * @since 2.9
 */
class StatusKaryawan extends Component
{
    private $statusKaryawan = [
        '1' => 'ORGANIK',
        '2' => 'OUTSOURCE',
    ];

    public function listStatusKaryawan()
    {
        return $this->statusKaryawan;
    }

    
}
