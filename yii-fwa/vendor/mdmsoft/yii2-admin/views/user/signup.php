<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Unit1;
use app\models\Unit2;
use app\models\Kota;
use mdm\admin\components\StatusKaryawan;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = Yii::t('rbac-admin', 'Daftar');


$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-user form-control-feedback'></span>"
];
$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-home form-control-feedback'></span>"
];
$fieldOptions3 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];
$fieldOptions4 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];

?>

<div class="login-box">    
    <div class="login-box-body">
        <div class="text-center" style="padding-top:15px">
            <?= Html::img('@web/img/jawara.png', ['alt'=>'Jawara', 'style'=>'width: 200px; margin-bottom: 15px']);?>
        </div>
        <h4 class="login-box-msg">Buat Akun Baru</h4>


        <?php $form = ActiveForm::begin([
                'id' => 'signup-form', 
                'enableClientValidation' => true,
                'enableAjaxValidation' => false,
            ]); ?>

        <?= $form
            ->field($model, 'nik', $fieldOptions1)
            ->label(false)
            ->textInput(['placeholder' => 'NIK atau 6 angka terakhir no. HP']) ?>
        
        <?= $form
            ->field($model, 'nama', $fieldOptions1)
            ->label(false)
            ->textInput(['placeholder' => 'Nama Lengkap']) ?>

        <?= $form
            ->field($model, 'unit1_id')
            ->label(false)
            ->dropdownList(ArrayHelper::map(Unit1::find()->all(),'id','unit1'),
                ['prompt'=>'pilih lokasi kerja']); ?>
        
        <?= $form
            ->field($model, 'unit2_id')
            ->label(false)
            ->dropdownList(ArrayHelper::map(Unit2::find()->all(),'id','unit2'),
                ['prompt'=>'pilih lokasi gedung']); ?>
        
        <?= $form->field($model, 'email',$fieldOptions3)
                 ->textInput(['placeholder' => 'Email'])
                 ->label(false)

        ?>

        <?= $form->field($model, 'password',$fieldOptions4)
                 ->passwordInput(['placeholder' => 'Password']) 
                 ->label(false)

        ?>

        <?= $form->field($model, 'retypePassword',$fieldOptions4)
                 ->passwordInput(['placeholder' => 'Ulangi Password']) 
                 ->label(false)
        ?>
        
        <div style="padding-top:10px">

        <?= Html::submitButton('Buat Akun',
            ['class' => 'btn btn-success btn-block','name' => 'login-button', 'tabindex' => '4']) 
        ?>
        <?php ActiveForm::end(); ?>   

        </div>
    </div>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->