<?php

namespace mdm\admin\components;


/**
 * Description of UserStatus
 *
 * @author Misbahul D Munir <misbahuldmunir@gmail.com>
 * @since 2.9
 */
class StatusKaryawan 
{
    const ORGANIK   = 1;
    const OUTSOURCE = 2;  
}
