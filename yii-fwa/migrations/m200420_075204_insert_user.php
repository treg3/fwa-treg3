<?php

use yii\db\Migration;

/**
 * Class m200420_075204_insert_user
 */
class m200420_075204_insert_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        // $this->execute("INSERT INTO `user` VALUES
        // (NULL,'660348','MOHAMAD KHAMDAN','7','1','1','sv9FbV9DsYrNfOzrPHz6MvnlDUj_0G4F',".
        // '\'$2y$13$GmQZn6rQ4rRI7Ear9zH0fOW7eUc1fJ4N1vguwfrm3LR7d.F3Vihoy\''.",NULL,'660348@telkom.co.id',
        // '10','1587363113','1587363113')");

        $this->execute("INSERT INTO `user` VALUES
        (660348,MOHAMAD KHAMDAN,7,1,1,sv9FbV9DsYrNfOzrPHz6MvnlDUj_0G4F,".'\'$2y$13$GmQZn6rQ4rRI7Ear9zH0fOW7eUc1fJ4N1vguwfrm3LR7d.F3Vihoy\''.",NULL,660348@telkom.co.id,10,1587363113,1587363113)");


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200420_075204_insert_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200420_075204_insert_user cannot be reverted.\n";

        return false;
    }
    */
}
