<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modelDiariumSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="diarium-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nik_id') ?>

    <?= $form->field($model, 'unit1_id') ?>

    <?= $form->field($model, 'unit2_id') ?>

    <?= $form->field($model, 'lokasi_id') ?>

    <?php // echo $form->field($model, 'kondisi_id') ?>

    <?php // echo $form->field($model, 'keterangan') ?>

    <?php // echo $form->field($model, 'submit_date') ?>

    <?php // echo $form->field($model, 'versi_app') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
