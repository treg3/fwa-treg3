<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Diarium;

/**
 * modelDiariumSearch represents the model behind the search form of `app\models\Diarium`.
 */
class DiariumSearch extends Diarium
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nik_id', 'unit1_id', 'unit2_id', 'lokasi_id', 'kondisi_id'], 'integer'],
            [['keterangan', 'submit_date'], 'safe'],
            [['versi_app'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Diarium::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'nik_id' => $this->nik_id,
            'unit1_id' => $this->unit1_id,
            'unit2_id' => $this->unit2_id,
            'lokasi_id' => $this->lokasi_id,
            'kondisi_id' => $this->kondisi_id,
            'submit_date' => $this->submit_date,
            'versi_app' => $this->versi_app,
        ]);

        $query->andFilterWhere(['like', 'keterangan', $this->keterangan]);

        return $dataProvider;
    }
}
