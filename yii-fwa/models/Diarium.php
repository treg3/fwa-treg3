<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "diarium".
 *
 * @property int $id
 * @property int $nik_id
 * @property int $unit1_id
 * @property int $unit2_id
 * @property int|null $lokasi_id
 * @property int|null $kondisi_id
 * @property string|null $keterangan
 * @property string $submit_date
 * @property float $versi_app
 *
 * @property Kondisi $kondisi
 * @property Kondisi $kondisi0
 * @property Lokasi $lokasi
 * @property User $nik
 * @property Unit1 $unit1
 * @property Unit2 $unit2
 */
class Diarium extends \yii\db\ActiveRecord
{
    public $today;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'diarium';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nik_id', 'unit1_id', 'unit2_id', 'submit_date', 'versi_app'], 'required'],
            [['nik_id', 'unit1_id', 'unit2_id', 'lokasi_id', 'kondisi_id'], 'integer'],
            [['submit_date','today'], 'safe'],
            [['versi_app'], 'number'],
            [['keterangan'], 'string', 'max' => 255],
            [['kondisi_id'], 'exist', 'skipOnError' => true, 'targetClass' => Kondisi::className(), 'targetAttribute' => ['kondisi_id' => 'id']],
            [['kondisi_id'], 'exist', 'skipOnError' => true, 'targetClass' => Kondisi::className(), 'targetAttribute' => ['kondisi_id' => 'id']],
            [['lokasi_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lokasi::className(), 'targetAttribute' => ['lokasi_id' => 'id']],
            [['nik_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['nik_id' => 'nik']],
            [['unit1_id'], 'exist', 'skipOnError' => true, 'targetClass' => Unit1::className(), 'targetAttribute' => ['unit1_id' => 'id']],
            [['unit2_id'], 'exist', 'skipOnError' => true, 'targetClass' => Unit2::className(), 'targetAttribute' => ['unit2_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nik_id' => 'Nik ID',
            'unit1_id' => 'Unit1 ID',
            'unit2_id' => 'Unit2 ID',
            'lokasi_id' => 'Lokasi ID',
            'kondisi_id' => 'Kondisi ID',
            'keterangan' => 'Keterangan',
            'submit_date' => 'Submit Date',
            'versi_app' => 'Versi App',
            'today' => 'Pilih tanggal',
        ];
    }

    /**
     * Gets query for [[Kondisi]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKondisi()
    {
        return $this->hasOne(Kondisi::className(), ['id' => 'kondisi_id']);
    }

    /**
     * Gets query for [[Lokasi]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLokasi()
    {
        return $this->hasOne(Lokasi::className(), ['id' => 'lokasi_id']);
    }

    /**
     * Gets query for [[Nik]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNik()
    {
        return $this->hasOne(User::className(), ['nik' => 'nik_id']);
    }

    /**
     * Gets query for [[Unit1]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUnit1()
    {
        return $this->hasOne(Unit1::className(), ['id' => 'unit1_id']);
    }

    /**
     * Gets query for [[Unit2]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUnit2()
    {
        return $this->hasOne(Unit2::className(), ['id' => 'unit2_id']);
    }
}
